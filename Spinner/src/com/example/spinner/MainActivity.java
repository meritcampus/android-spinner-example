package com.example.spinner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity implements OnItemSelectedListener{
Spinner spinner;
TextView textview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		spinner= (Spinner)findViewById(R.id.spinner);
		textview =(TextView) findViewById(R.id.spinner_text);
		String country[] = {"India","Srilanka","Pakistan","Australia","SouthAfrica","newzealand","Bangladesh"};
		ArrayAdapter<String> adapter  = new ArrayAdapter<>(getApplicationContext(), android.R.layout.test_list_item, country);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
	}
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		//Toast.makeText(getApplicationContext(), "Clicked on "+spinner.getSelectedItem().toString() + " Item Position " + position, Toast.LENGTH_SHORT).show();
		textview.setText(spinner.getSelectedItem().toString());
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
		
	}

	
}
